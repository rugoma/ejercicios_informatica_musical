\version "2.18.2"
\language "espanol"

\header {
  title = "Peer Gynt"
  subtitle = "Solveig‘s song"
}

\paper {
  #(set-paper-size "a4")
}

global = {
  \key do \major
  \time 4/4
  \tempo 4=76
}

violin = \relative do'' {
  \global
  % La música continúa aquí.
  \override Score.BarNumber.break-visibility = #'#(#t #t #t)
  r1
  r2 r4 mi,\p\upbow la( si8 do) re4( mi8\< fa)
  fa(\> mi)\! mi do la4( la8\< do)
  do(->\> si)\! si sols sols16\upbow mi8.\upbow~ mi4~
  mi8 r8 r2 mi4\downbow( % Fin 1er pentagrama
  sol8--) sol--( si--) do-- re4 mi8--(\downbow fa--\upbow)
  fa8( mi) mi do la4 la8\< do\! 
  do->(\f si) si\upbow sol\upbow sol16->( mi8.~\> mi4~
  mi8)\! r8 r2 r8 mi8\p\upbow
  mi( sols si do) la4( mi'8 res) % Fin 2o pentagrama
  si4( re?8 dos) la4. la8-- do?8( si) si la mi'4. mi,8\upbow
  mi8\cresc( sols)\! si-- do-- la( mi') mi-- res-- 
  si--\f-> si-- re?-- dos-- la4.\> la8\!
  do?4.\p-> mi8 si4.-> la8 % Fin 3er pentagrama
  la2~ la8 r\fermata mi'4\fermata(~
  \key la \major
  \time 3/4
  \tempo 4=120
  mi8. mi16 mi8. re16 dos8. re16 
  mi8-.) r mi2(~ mi8. fas16 mi8. re16 dos8. re16 mi2.)
  \override Score.BarNumber.break-visibility = #'#(#f #f #f)
  \bar "|."
  
}

\score {
  \new Staff \with {
    instrumentName = "Violín"
  } \violin
  \layout { }
}
