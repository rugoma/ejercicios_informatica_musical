\version "2.18.2"
\language "espanol"

\header {
  title = "08"
  subtitle = "Escritura avanzada para guitarra"
}

\paper {
  #(set-paper-size "a4" 'landscape)
}

global = {
  \key re \major
  \numericTimeSignature
  \time 6/8
  \tempo 4=145
  \partial 4.
}

Ncuerda_arriba =
#(define-music-function (parser location StringNumber) (string?)
  #{
    \override TextSpanner.style = #'solid
    \override TextSpanner.font-size = #-5
    \override TextSpanner.direction = #1
    \override TextSpanner.bound-details.left.stencil-align-dir-y = #CENTER
    \override TextSpanner.bound-details.left.text = 
      \markup { \circle \number #StringNumber }
    \override TextSpanner.bound-details.right.text =
      \markup { \draw-line #'(0 . -2) }
  #})

Ncuerda_abajo =
#(define-music-function (parser location StringNumber) (string?)
  #{
    \override TextSpanner.style = #'solid
    \override TextSpanner.font-size = #-5
    \override TextSpanner.direction = #1
    \override TextSpanner.bound-details.left.stencil-align-dir-y = #CENTER
    \override TextSpanner.bound-details.left.text = 
      \markup { \circle \number #StringNumber }
    \override TextSpanner.bound-details.right.text =
      \markup { \draw-line #'(0 . 2) }
  #})

upper_uno = \relative do'' {
  \global
  % La música continúa aquí.
  r4.
  <sib re>4.
  \Ncuerda_arriba "1"
  sol'8(-4 \startTextSpan mi) fas16( sol fas mi \stopTextSpan re4)
}

lower_uno = \relative do'' {
  \global
  % La música continúa aquí.
  r8 la, la mi'-1( re) sib-1_\5
  \Ncuerda_abajo "4" \textSpannerDown <re' sib mi,_1>4 \startTextSpan
  re,8~ re4. \stopTextSpan 
  
}

partefinal = \relative do'' {
  
  <<
    \new Voice { 
      \voiceOne
      re4.\rest %fin C.1
      \Ncuerda_arriba "1"
      sol8-2^\markup{"VII"}^\markup {
        \fret-diagram #"s:1;4-o;3-7;2-8;1-7;" 
      } \startTextSpan 
      sol sol si-1[ re-4 \stopTextSpan la-1~] la fas4-3 si,4.\rest \bar "|."
    }
    \new Voice {
      \voiceTwo
      r8 mi\rest mi\rest %fin C.1
      <re re,>2 r4 s2.
    }
    \new Staff = "bajo" {
      \once \omit Staff.TimeSignature
      s8 \change Staff = "alto" {la,}
      \change Staff = "bajo" la
      <<
        { re'4.\2 si4\2 re8_\3~ re8 re\rest re\rest re4.\rest}
        \\
        {la,2._\5 s}
        \\
        { \stemDown si'4.\3 sol4\3 la8_\4~ la re,_\4 re re4. }
     >>
    }
  >>
}

\score {
  \new GrandStaff \with {
    instrumentName = "Guit. 1"
  } {
    \new Staff = "alto" {
      \clef "treble" 
      <<
        \upper_uno \\ \lower_uno >>
      \partefinal
    }
  }
  \layout {}
}