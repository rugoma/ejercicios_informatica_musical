\version "2.18.2"
\language "espanol"

\header {
  title = "02"
  subtitle = "Nana"
  composer = "Cancionero musical español"
  arranger = "Felipe Pedrel"
  poet = "Recogida por: D. Eduardo Ocón"
}

\paper {
  #(set-paper-size "a4")
}

global = {
  \key re \major
  \time 3/4
}

soprano = {
  \relative do' {
    \global
    fas2 fas4 fas fas las %fin compás 2
    \acciaccatura si8 dos2 \acciaccatura si8 las4~ las2. % fin compás 4
    dos4 re dos si2 \grace {\tuplet 3/2 {si16 las sols}} las4~ % fin 1er pentagrama
    las2. las4 si dos \grace {dos16 re} dos4. si8 las4~ 
    las \acciaccatura sols8 fas2 % fin compas 10
    sols4 las si las2 \acciaccatura sols8 fas4~ fas2. % final
  }
}

contraalto = {
  \relative do' {
    \global
    r4 r dos~ dos re fas sol2 fas4~ fas2. las4 si la fas sols fas % fin 1er pentagrama
    fas2. fas4 mi sol sol2 fas4 fas dos2 fas2.~ fas2 dos4~ dos2. % final
  }
}

tenor = {
  \relative do' {
    \global
    \clef bass
    r4 r dos~ dos si dos mi2 dos4~ dos2. fas2 fas4 re2 dos4~ % fin 1er pentagrama
    dos2. dos4 si mi mi re dos~ dos las2 si4 dos re dos2 las4 si las2 % final
  }
}

bajo = {
  \relative do {
    \global
    \clef bass
    fas2 fas4~ fas fas2 mi fas4~ fas2. fas4 si, fas' si2 fas4 % fin 1er pentagrama
    fas2. fas4 sol mi mi2 fas4~ fas fas2 fas2.~ fas2 fas4~ fas2. % final
  }
}

letra = \lyricmode {
  Duer me ni ño chi qui to __ duer me mi al ma __
  duer me te lu ce ri __ to de la ma ña na
}


\score {
  \new ChoirStaff <<
    \new Staff <<
      \new Voice = "Soprano" {
        \voiceOne
        \soprano
      }
      \new Lyrics \lyricsto "Soprano" {
        \letra
      }
      \new Voice = "Contraalto" {
        \voiceTwo
        \contraalto
      }
    >>
    \new Staff <<
      \new Voice = "Tenor" {
        \voiceOne
        \tenor
      }
      \new Voice = "Bajo" {
        \voiceTwo
        \bajo
      }
    >>
  >>
  \layout { }
  \midi { }
}
