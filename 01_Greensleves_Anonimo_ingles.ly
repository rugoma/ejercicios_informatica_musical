\version "2.18.2"
\language "espanol"

\header {
  title = "01"
  subtitle = "Greensleeves"
  composer = "Anónimo inglés"
}

\paper {
  #(set-paper-size "a4")
}

global = {
  \key sol \major
  \time 6/8
  %\tempo 4=100
  \partial 8
}

derecha = \relative do'' {
  \global
  % La música continúa aquí.
  mi,8 sol4 la8 si8. dos16 si8 %Compás 1
  la4 fas8 re8. mi16 fas8 % Compás 2
  sol4 mi8 mi8. res16 mi8 % Fin 3er compás
  fas4 res8 si4 mi8 sol4 la8 si8. dos16 si8 % Fin del primer pentagrama
  la4 fas8 re8. mi16 fas8 sol8. fas16 mi8 res8. dos16 res8 mi4 mi8 mi4. % Fin del compás 8
  re'4. re8. dos16 si8 la4 fas8 re8. mi16 fa8 % Fin del segundo pentegrama
  sol4 mi8 mi8. res16 mi8 fas4 res8 si4 r8 re'?4. re8. dos16 si8 la4 fas8 re8. mi16 fas8 % Fin del compás 14
  sol8. fas16 mi8 res8. dos16 res8 mi2. % Fin de la mano derecha

}

izquierda = \relative do {
  \global
  % La música continúa aquí.
  r8 mi sol si sol si re re, fas la re, fas la mi sol si mi, sol si % fin compas 3
  si, res fas si,4. mi8 sol si sol si re? % fin primer pentagrama
  
  re, fas la re, fas la mi sol si si, res fas mi sol si mi,4. % fin compas 8
  sol8 si re sol, si re re, fas la re, fas la % Fin segundo pentagrama
  do, mi sol do,4. si8 res fas si,4. sol'8 si re? sol, si re % fin compas 13
  re, fas la re, fas la do, mi sol si, res fas mi sol si mi,,4.\bar "|." % fin de la mano izquierda
  
}

\score {
  \new PianoStaff \with {
    instrumentName = ""
  } <<
    \new Staff = "derecha" \with {
      midiInstrument = "acoustic grand"
    } \derecha
    \new Staff = "izquierda" \with {
      midiInstrument = "acoustic grand"
    } { \clef bass \izquierda }
  >>
  \layout { }
  \midi { }
}