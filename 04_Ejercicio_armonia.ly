\version "2.18.2"
\language "espanol"

\header {
  subsubtitle = "Realiza la siguiente melodía de manera que module a las 5 tonalides vecinas"
}

\paper {
  #(set-paper-size "a4" 'landscape)
}

global = {
  \key la \major
  \time 4/4
}

right = \relative do'' {
  \global
  % La música continúa aquí.
  s1*9
}

left = \relative do' {
  \global
  % La música continúa aquí.
  r4 mi, sols la, mi' re dos2 re res mi mis fas mi? % fin compás 5
  re4 res mi fas mi2 fas mi4 dos re mi la,1 \bar "|."%final
}

cifrado = \figures {
      \figuremode {
        <_>4 <_> <_> <6 _5> <7 _\+> <4\+> <6> <6 _5/> % fin compás 2
        <6>2 <6>4 <6 _5/> <_>2 <6>4 <6 _5/> % fin compás 4
        <_>2 <2>4 <4\+> <6> <6\+> <6> <6\+> <_>2 <7>4 <6\+> % fin compás 7
        <_> <6 _5/> <_> <7 _\+> <_>1 % final        
      }
}

texto = \lyricmode {
  \markup {\box \pad-markup #0.2 "LaM"}4 "V"4 "I"4 
  "VI"4 "V"4
  ""4*5
  "V"4*2
  "I"4*2
  ""4*5
  "V"4
  "I"4 ""4*2 "V"4 "I"4*2
  "II"4 "V"4 "I" "" "IV" "V" "I"1
}

samplePath =
  #'((moveto 0 0)
     (lineto 5 0)
     (lineto 5 1)
     (lineto 5.01 1)
     (lineto 5.01 0)
     (lineto 0 0)
     (closepath))


flechas = \lyricmode {
   \markup {
     \center-column {
        %\arrow-head axis (integer) dir (direction) filled (boolean)
        \arrow-head #Y #UP ##f
        %\draw-dashed-line dest (pair of numbers)
        \draw-dashed-line #'(0 . 10)
     }
        
   }4
  
  \markup {
    \path #0.25 #samplePath
  }4
   
   \override TextSpanner.bound-details.left.text =
    \markup { \draw-line #'(0 . 1) }
   \override TextSpanner.bound-details.right.text =
    \markup { \draw-line #'(0 . 2) }
   \once \override TextSpanner.bound-details.right.padding = #-2
   "V"4 \startTextSpan 
   "" "I" \stopTextSpan
   
}

\score {
  <<
    \new PianoStaff <<
      \new Staff = "right" \right
      \new Staff <<
        \new Voice = "left" { \clef bass \left }
        \cifrado
      >>
    >>
    \new Lyrics \with { \override LyricText.self-alignment-X = #LEFT }
    { \texto }
    \new Lyrics \with { \override LyricText.self-alignment-X = #LEFT }
    { \flechas }
  >>
  \layout { }
}
