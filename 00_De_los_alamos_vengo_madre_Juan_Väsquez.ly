
\version "2.18.2"
\language "espanol"

\header {
  title = "De los álamos vengo, madre."
  composer = "Juan Väsquez (1500-1560)"
}

\paper {
  #(set-paper-size "a4")
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

global = {
  \key sib \major
  \numericTimeSignature
  \time 4/4
}

clarinet = \relative do'' {
  \global
  \transposition sib
  % La música continúa aquí.
  r2 fa,4 sol sib4. sib8 sib4 sol8 la %fin 2º compás 
  sib do re mib re4 do sib re4~re8 do sib4 %fin 4º compás
  la4 sib~sib8 sol la4 sib2 r %fin 6º compás
  fa4 sol sib8 sib sol la sib do re2 do4 % fin 8º compás
  sib4 re~re8 do sib4 la sib~sib8 la la4  %fin 10º compás
  \time 3/2  
  sib2 r sib4 do 
  \time 4/4 
  re4 do8 do re re do4 % fin 12º compás
  sib2 r r1 %fin 14 compás
  r2 re4 mib fa8 mib re do sib la sol4 %fin 6º compás
  fa sib8 do re mib do do sib4 re~re8 do sib4% fin 18º compás
  la4 sib2 la4 sib2 r %fin 20º compás
  fa4 sol sib8[ sib] sol la sib do re2 do4 % fin compás 22
  sib re~ re8 do sib4 la sib~ sib8 la la4 %fin compás 24
  \time 3/2
  sib2 r r
  \time 4/4
  re4 mib fa mib8 mib re[ do] re mib re4 do8 sib %fin compás 27
  la4 sib sib mib, fa1 fa2 r2 %final
}

altoSax = \relative do'' {
  \global
  \transposition mib
  % La música continúa aquí.
  r1 r2 re,4 mib fa2 fa4 fa re2 sib %fin 4º compás
  do4 re2 do4 re2 r r re4 mib fa fa fa4. mib8 %fin 8º compás
  re2 sib4 sib do1 \bar "|" %fin 10º compás
  \time 3/2 
  sib2 r  re4 mib 
  \time 4/4 
  fa2 fa4 fa %fin 12ºcompás 
  re2 sib do4 re2 do4 %fin 14 compás
  re2 r r re4 mib % fin 16º compás
  fa4 fa fa4. mib8 re2 sib4 sib %fin 18º compás
  re1 sib2 r %fin 20º compás
  r2 re4 mib fa fa fa4. mib8 %fin compás 22
  re2 sib4 sib do1
  \time 3/2
  re2 r r
  \time 4/4
  r re4 mib fa2 fa4 mib %fin compás 27
  re2 sib2 do4 re do2 sib2 r %final
}

clarinetPart = \new Staff \with {
  instrumentName = "Cln"
  midiInstrument = "clarinet"
} \clarinet

altoSaxPart = \new Staff \with {
  instrumentName = "Sax c."
  midiInstrument = "alto sax"
} \altoSax

\score {
  <<
    \clarinetPart
    \altoSaxPart
  >>
  \layout { }
  \midi {
    \tempo 4=100
  }
}
