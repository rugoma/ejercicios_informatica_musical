\version "2.18.2"
\language "espanol"

\header {
  title = "03"
  subtitle = "6 danzas rumanas,nº 2"
  composer = "B. Bartók"
}


v_arriba = \relative do' {
   \clef treble
   \key do \major
   \time 2/4

   re8-\staccato\p\< mi-\staccato fa-\staccato sol-\staccato % Fin compás 1
   la-\staccato sol-\staccato re sol\! % Fin 2 compas
   la4-\staccato_\markup{tenuto} la8 r16 si~\> % Fin compas 3 
   si2 sol8-\staccato\p\! la-\staccato\< si-\staccato sol-\staccato % Fin compas 5
   do4-- \tuplet 5/4 {do16 si\! la sol fa} % Fin compas 6
   sol4-\staccato_\markup{tenuto} sol8 r16 la16\>(sol2) % Fin 1er pentagrama
   sol8\!-\staccato\< la-\staccato si-\staccato sol-\staccato % Fin compas 9
   do4-- \tuplet 5/4 {do16 si\! la sol fa} % Fin compas 10
   fa8 mi-\staccato fa-\staccato sol-\staccato %
   fa-\staccato mi-\staccato re4-- % Fin 12
   re8-\staccato mi-\staccato fa-\staccato la-\staccato %
   sol-\staccato fa-\staccato mi-\staccato fa-\staccato %
   re4-\staccato^\markup{rit.....} re8-\staccato r16 la( re2) % Final.
   
}

v_abajo = \relative do {
   \clef bass
   \key do \major
   \time 2/4
   %\set Staff.pedalSustainStrings = #'("Ped" "P-" "*")
   re,4-\staccato\sustainOn <fa' la>-\staccato\sustainOff re,-\staccato <sol' si>-\staccato % Fin c.2
   re,-\staccato <sol' re' fa>-\staccato <sol re' fa>2 sol,4-\staccato <re'' fa>-\staccato
   la,4-\staccato <do' fa>-\staccato %Fin c.5
   re,-\staccato <do' re> <si re>2 re,,4-\staccato <sol' re'>-\staccato % Fin c.9
   do,,-\staccato <si'' do>-\staccato <fa la do>-\staccato <la dos>-\staccato %Fin c.10
   <sib re>-\staccato <sib, fa'>-\staccato sol-\staccato <re' si'?>-\staccato % Fin c.12
   la-\staccato <sol' dos>-\staccato re,-\staccato <re' fa la>-\staccato %Fin c.
   <re fa la>2-\staccato
   \set Staff.forceClef = ##t
   \clef treble

}

\score {
    \new PianoStaff <<
       \set PianoStaff.instrumentName = #"Piano  "
    \new Staff = "" \v_arriba
    \new Staff = "" \v_abajo
  >>
  \layout { }
  \midi { }
}

