\version "2.18.2"
\language "espanol"

global = {
  \key do \major
  \numericTimeSignature
  \time 6/8
  \partial 8
  \tempo "Adagio" 4=80
}

right = \relative do'' {
  \global
  % La música continúa aquí.
  mi8 sols,4 sols8 sols(-\staccato la-\staccato si-\staccato) % C.1
  la4 la8 la4 mi'8( sols,4) sols8 sols(-\staccato la-\staccato si-\staccato) % C.2
  la4.-^( la4) sol?8( fas4) fas8 fas las dos % C.2
  si4 si8 si4 la?8 sol fas mi si'4 si,8 mi4.-^( mi4) % \bar "|."
}

left = \relative do' {
  \global
  % La música continúa aquí.
  r8 % Anacrusa
  <si re>4 <si re>8 <si re>4. <do mi>4 <do mi>8 <do mi>4. % C.2
  <si re>4 <si re>8 <si re>4. <do mi>4.~-^ <do mi>4
  \clef bass
  <si re>8( <las dos>4) <las dos>8 <las dos>4.
  <si res>4 <si res>8 <si res>4 si8
  mi4( sol,8) si4 si,8 mi4.~-^ mi4 \bar "|." % Final
}

\score {
 
  \new PianoStaff 
  <<
    \new Staff = "right" \with {
      midiInstrument = "acoustic grand"
    } \right
    \new Staff = "left" \with {
      midiInstrument = "acoustic grand"
    } { \clef bass \left }
  >>
  
  \header {
  title = "Análisis musical"
  subtitle = "M. A. Reizábal"
  subsubtitle = "Ejemplo IV.28"
  composer = "R. Schumann (1810-1856)"
  piece = "Album de la juventud"
  }

  \layout { }
  \midi {
    \tempo 4=80
  } 
}

%% Fragmento Nocturno Chopin
%% Título: Ejemplo IV.30
%% Pieza: Nocturno Op.32, nº2
%% Compositor: F. Chopin (1810-1849)

global_chopin = {
  \key lab \major
  \numericTimeSignature
  \time 4/4
  \tempo 4=80
}

derecha = \relative do {
  \global_chopin
  \override TextSpanner.style = #'line
  \override TextSpanner.bound-details.left.text =
    \markup { \draw-line #'(0 . -2) }
  \override TextSpanner.bound-details.right.text =
    \markup { \draw-line #'(0 . -2) }
  
  <do'' lab mib do>2\arpeggio\p(^\markup { "INTRODUCCIÓN" } \startTextSpan
  % Este fragmento va a tres voces 
  << {
      lab4. sib8
     }
     \\
     {
       reb,2
     }
     \\
     {
       fa4\< fab\!
     }
  >>
  <do' lab mib do>1\arpeggio\fermata)\> \stopTextSpan \bar "||" %Fin intro 
  do4\<(_\markup { \italic "sempre piano e legato"} si do mib % Fin c.3
  do)\> do8.( sib16 lab4) mib( %Fin c.4 
  do8\< mib lab sib do lab\! \tuplet 3/2 {sol' fa mib)} %Fin 1er penta.
  sib4( \tuplet 7/4 {la16 sib dob sib reb dob sib)} sib4\< si(\!
  do si do mib) \bar "|."
}

izquierda = \relative do {
  \global_chopin
  <lab' mib lab,>2\p\arpeggio <lab reb,> <lab mib lab,>1\arpeggio %Fin intro
  \tuplet 3/2 {lab,8 <lab' do> mib} \tuplet 3/2 {lab,8 <la' sib> re,}
  \tuplet 3/2 {lab8 <la' do> mib} \tuplet 3/2 {lab,8 <sol' reb'> mib}  % Fin c.3
  \tuplet 3/2 {lab,8 <lab' do> mib} \tuplet 3/2 {lab,8 <sol' reb'> mib}
  \tuplet 3/2 {lab,8 <lab' do> mib} \tuplet 3/2 {lab,8 sib'? mib,}  % Fin c.4
  \tuplet 3/2 {lab,8 lab' mib} \tuplet 3/2 {lab,8 <lab' do> mib}
  \tuplet 3/2 {lab,8 <lab' do> mib} \tuplet 3/2 {do8 mib' mib,}  % Fin c.5
  <<
    {s4}
    \\
    {\tuplet 3/2 {do,8 mib'' sol,} \tuplet 3/2 {reb8 fab' lab,\<}}
  >>
  <<
    {fab4} 
    \\ 
    \tuplet 3/2 {fab8\> reb' lab\!}
  >>
  << 
    {mib4} 
    \\ 
    \tuplet 3/2 {mib8 reb' sol,}
  >> % Fin c.6
  \tuplet 3/2 {lab,8 <lab' do> mib} \tuplet 3/2 {lab,8 <la' sib> re,}
  \tuplet 3/2 {lab8 <la' do> mib} \tuplet 3/2 {lab,8 <sol' reb'?> mib}  % Final
}

\score {

  \new PianoStaff
  <<
    \new Staff = "right" \with {
      midiInstrument = "acoustic grand"
    } \derecha
    \new Staff = "left" \with {
      midiInstrument = "acoustic grand"
    } { \clef bass \izquierda }
  >>
    \header {
     piece = "Nocturno Op. 32, nº 2"
     subsubtitle = "Ejemplo IV.30"
     composer = "F. Chopin (1810-1849)"
  }

}

\paper { print-all-headers = ##t }